\section{FPGA-based Fault Injection}
\label{sec:ffi}

FFI techniques have significantly advanced during last two decades, focusing on speeding up the execution of fault injection experiments, and reducing the number of experiments required to assess the robustness of the implementation. This sections details the main advances in these two fronts.

\subsection{Speeding up FPGA-based Fault Injection}

Modern FPGAs benefit from their dynamic partial reconfiguration capabilities to internally access and manipulate their own configuration memory (CMEM) to emulate the effect of faults. This prevents the high intrusiveness of early works~\cite{civera2002fpga}, which relied on instrumenting the netlist to provide a fault injection/removal interface, and the communication bottleneck of posterior works~\cite{de2008fault} in which a PC controlled the partial reconfiguration of the FPGA.

Most of the recent works in this field make use of Xilinx's Internal Configuration Access Port (ICAP)~\cite{VivadoLibraries} to emulate the occurrence of SEUs in CMEM. Some works~\cite{ramos2017characterizing} rely on proprietary IP soft-cores, like Xilinx's Soft Error Mitigation (SEM) core~\cite{SEM}, to control the injection of SEUs in the design under test (DUT). Other works~\cite{nunes2015fired}~\cite{sari2017flexible} have developed a custom fault injection infrastructure, usually based on Xilinx's Microblaze soft-core processor, to have a more precise control over the fault injection process and define more elaborated injection scenarios than with proprietary IPs. As~\cite{cardona2015ac_icap} showed, a complete reimplementation of ICAP libraries can provide a faster and more fine grained management of the CMEM. Despite their benefits, common disadvantages of on-chip fault injectors operating through ICAP are that i) they share the reconfigurable fabric with the DUT, reducing the resources available for the target implementation, and ii) they require additional constraints to isolate the injector and the DUT to prevent occasional interferences. 

Some proposals~\cite{villata2014fast} rely on the use of the Xilinx's Processor Configuration Access Port (PCAP)~\cite{ZynqTRM}, instead of ICAP, to access the CMEM from the hardwired processor core of ZYNQ devices. This significantly reduces the interference of the injector with the DUT and accelerates the injection process, specially when PCAP drivers are also reimplemented~\cite{vipin2014zycap}. 

Finally, using stacks of FPGA boards can be used to execute fault injection experiments in parallel~\cite{Fleming18}. Even though this approach does not accelerates individual experiments, it has the potential of reducing the total experimentation time depending on the number of available boards.

\subsection{Reducing the Number of Experiments}

The total number of fault injection experiments can be reduced by using manufacturers specific technologies, like Xilinx's essential bits, and/or generic approaches, like statistical fault injection.

\subsubsection{Xilinx's Essential Bits Technology}
\label{sec:essentialbits}

Any given design implemented on an FPGA is unlikely to use all available logic resources and just a small fraction of all routing elements. Accordingly, blindly injecting faults in all the configuration memory cells of the device will probably lead to lots of useless experiments, as many target bits cannot impact in any way the behaviour of the DUT. For instance, the Zynq XC7Z020 device contains 10000 frames, with 101 words of 32 bits each. Thus, $32.32$ millions of experiments are required to target all possible CMEM bits.

Xilinx's Essential Bits technology~\cite{Le12} provides a mask file (.msk) identifying those bits, within the CMEM, that will modify the design circuitry after being altered (\emph{essential bits}. Nevertheless, this does not mean that the behaviour of the design will be affected when those bits are flipped, so fault injection experiments are still required to determine the impact of the faults. The subset of the essential bits that may lead the system to a failure is known as \emph{critical bits}. In such a way the number of fault injection experiments may be greatly reduced to target just the essential bits, with smaller circuits getting better speed-up ratios than large ones.

\subsubsection{Statistical Fault Injection}

\emph{Statistical fault injection} was formalised at~\cite{leveugle2009statistical} and lately enhanced in~\cite{Tuzov18} to keep affordable the time required for fault injection campaigns. It estimates the characteristics (robustness metrics) of the whole population $N$ (CMEM cells $\times$ workload clock cycles) by sampling just a small subset of individuals. By assuming a certain confidence level $t$ ($1.95$ corresponds to a typical 95\% confidence interval), and the probability $p$ of an individual to exhibit one of the considered characteristics ($0.5$ is the worst-case scenario), the required sample size $n$ to get an error margin $e$ (e.g. $0.05$) can be computed by equation~\ref{eq:samplesize}.  

\begin{equation}
	n = \frac{N}{1+e^2 \times \frac{N-1}{t^2 \times p \times (1-p)}}
	\label{eq:samplesize}
\end{equation}

For instance, in the worst-case scenario, when $p = 0.5$ and all $32.32$ millions of bits of the Zynq XC7Z020 device are targeted, just by sampling 385 individuals the robustness metrics could be estimated with a $5\%$ margin of error and a $95\%$ confidence interval. However, carrying out too few experiments may compromise the representativeness of reported results and derived conclusions. In case of dealing with critical systems, a narrower margin of error of $0.1\%$ is usually required to get an accurate estimation of their robustness, and thus the sample size would be of $924718$ individuals for large populations (usually designs implemented on FPGAs would get more than 1 million of essential bits).


\subsubsection{Combining both approaches}

Both approaches can be easily combined by first reducing the number of target bits to those identified as essential bits, and then this subset of bits is sampled using the statistical fault injection approach.

Listing~\ref{sampling} shows a high level definition of the fault injection process to be followed to randomly target essential bits provided by a mask file until the required sample size (for a given margin of error) is reached.

\begin{lstlisting}[escapeinside=||,label=sampling,numbers=none,captionpos=b,caption=Combined sampling fault injection strategy, float]
|\includegraphics[width = \columnwidth]{Figures/Injector_Sampling_Listing_V3.pdf}|
\end{lstlisting}

%Nevertheless, running nearly one million of fault injection experiments for each considered implementation in a DSE process could still be unaffordable. Accordingly, some extra effort is still required to reduce, as much as possible, the number of required experiments.